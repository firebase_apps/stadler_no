module Main exposing (..)

import Browser 

import Css exposing (..)
import Browser.Navigation as Nav
import Url
import Html.Styled exposing (..)
import Html.Styled.Events exposing (..)
import Html.Styled.Attributes exposing (..)
import Html exposing (i)

---- MODEL ----

type Page = Home
    | Projects

type alias Model =
    {
    url: Url.Url
    , key: Nav.Key
    , showMenu: Bool
    }



init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
  ( Model url key False, Cmd.none )

---- UPDATE ----

type Msg
    = NoOp
    | LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | ToggleMenu 


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
    LinkClicked urlReq -> case urlReq of
                              Browser.Internal url ->
                                ( model, Nav.pushUrl model.key (Url.toString url) )

                              Browser.External href ->
                                ( model, Nav.load href )

    UrlChanged url -> ( { model | url = url, showMenu = False }, Cmd.none )
    ToggleMenu -> ({model | showMenu = not model.showMenu}, Cmd.none)
    _ -> (model, Cmd.none)






subscriptions : Model -> Sub Msg
subscriptions _ =
  Sub.none


---- VIEW ----

view : Model -> Browser.Document Msg
view model =  
  let content = 
        if model.showMenu then navBar else
        case model.url.path of
            "/"-> homePage 
            "/menu" -> navBar
            "/projects" -> projectsPage
            "/led" -> ledProjectPage 
            "/nixops" -> nixopsProjectPage
            "/blogs" ->  blogsPage
            "/blogs/reading_list" -> readingListPage

            _ -> text "not found"

      body = div []
                [
                  burgerMenu model.showMenu
                  ,content 
                ]
  in
    {title = "Stadler no"
    , body = [ Html.Styled.toUnstyled body ]
    }


main : Program () Model Msg
main =
  Browser.application
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    , onUrlChange = UrlChanged
    , onUrlRequest = LinkClicked
    }



theme : { secondary : Color, primary : Color, primText: Color, highText: Color }
theme =
    { primary = hex "f2b02c"
    , secondary = rgb 250 240 230
    , primText = rgba 255 255 255 0.67
    , highText = rgba 255 255 255 0.80
    }




homePage : Html Msg
homePage = div [ class "centered"] 
            [ 
                styledH1 "Aksel Stadler"
                , div [css [color theme.primText, fontSize (px 14), padding (px 0)]] [text "Robotics Engineer & Programmer"]
            ]

projectsPage : Html Msg
projectsPage = div [] 
                [
                    pageTitle "Projects" 
                    , ledProjectIntro
                    , nixopsProjectIntro
                    , koronaProjectIntro
                ]


ledProjectPage : Html Msg
ledProjectPage = div [][
                    pageTitle "LED Thermometer for Africa Burn"
                    ,ledProject
                    ]

nixopsProjectPage : Html Msg
nixopsProjectPage = div [] [
                    pageTitle "Liveview & Nixops"
                    ,nixopsProject
                    ]

blogsPage: Html Msg
blogsPage = div [] [
                    pageTitle "Blogs"
                    , a [href "/blogs/reading_list"] [text "Reading list"]
                    ]

navBar : Html Msg
navBar =  div [class "nav-links"]
  [
    a [ href "/"] [ text "[ Home ]"] 
  , a [ href "/projects"] [ text "[ Projects ]"] 
  , a [ href "/blogs"] [ text "[ Blogs ]"] 

  ]


burgerMenu : Bool-> Html Msg
burgerMenu showMenu = 
  let icon = if showMenu then "clear" else "menu"
  in
    button [css [
          position fixed, right (px 20), top (px 20), backgroundColor (rgba 0 0 0 0), border (px 0), outline none, active [outline none], focus [outline none]
         ], 
    onClick ToggleMenu] 
    [
      Html.Styled.i [class "material-icons", css [fontSize (px 30), color theme.primText]] [text icon]
    ]


styledH1 : String -> Html Msg
styledH1 string = h1 [css [fontWeight (int 400), color theme.primary, fontSize (px 30), marginBottom (px 0)]] [text string]

pageTitle : String -> Html Msg
pageTitle string = div[] [
                    h1 [css [fontWeight (int 400), fontSize (px 22), color theme.primary]] [text string]
                    , hr [css [border (px 1), borderColor theme.primary, borderStyle solid, borderTop (px 0), padding (px 0)]] []]

subTitle : String -> Html Msg
subTitle string = h2 [css [color theme.primary, fontSize (px 18), padding (px 0)]] [text string]


readingListPage : Html Msg
readingListPage = 
    div [] 
    [
        pageTitle "Reading list"
        ,p [] [text "Books i've read, and aspire to read. Value while reading is the momentary enterntainement (1-10). Value after reading is how much i've referred to the book in a conversation with a diverse crowd (1-10). (Stressing diverse here, as my brother and I can discuss wheel of time for hours, easily)"]
        ,Html.Styled.table [style "width" "100%", css[borderCollapse collapse, color theme.primText]] 
            [
                tr [] 
                [
                    styledTh "Book/Series"
                    ,styledTh "Value while reading"
                    ,styledTh "Value after reading "
                    , styledTh "Key takeof"
                ],
                tdTrio "Wheel of Time" "9" "2"
                ,tdTrio "Sapiens/ Deus" "6" "10"
                ,tdTrio "A Fine Balance" "7" "7"
                ,tdTrio "Grensen" "7" "7"
                ,tdTrio "The Journey back to you" "5" "6"
                ,tdTrio "Our Mathematical Universe" "7" "8"
                ,tdTrio "The Journey back to you" "5" "6"
                ,tdTrio "A Brief History of Time" "6" "6"
                ,tdTrio "Kingkiller Chronicles" "9" "3"



            ]
    ]
ledProjectIntro : Html Msg
ledProjectIntro = 
  projectSection "/led" "saunandtermo.png" 
  "LED thermometer for Africa burn"
  "In Africa burn 2019 we gifted a sauna to the community. We installed a 3m tall LED thermometer so bypassers could see  the current sauna temperature. Here is a walkthrough of the code and how I wired it all up "



nixopsProjectIntro : Html Msg
nixopsProjectIntro = 
    projectSection "/nixops" "nixops.png"
    "Liveview and Nixops"
    "Phoenix liveview is the new go-to framework for building SPAs in much the same way that 2021 is the year of the desktop (fingers crossed). Its killer feature is server-side-rendered dynamic webpages (it's as great as it sounds). Nixops is for people that are too cool for kubernetes"

koronaProjectIntro : Html Msg
koronaProjectIntro = 
    projectSection "https://kronavenn.web.app" "telefon.svg"
    "Koronavenn"
    """A service made during the 2020 pandemic. The purpose
    was to connect quaranteened people so everyone had 
    a call buddy or a "corona-friend" (which is the title in Norwegian)"""

styledP : String -> Html Msg
styledP string = p [] [text string]

styledC : String -> Html Msg
styledC string = Html.Styled.pre [] [code [] [text string]]

projectSection : String -> String -> String -> String -> Html msg
projectSection link imageUrl title content  = 
  div [class "row center-xs", css [alignItems center]] 
    [
      div [class "col-xs-12 col-md-6"]
        [
          img [style "width" "100%" , style "background-color" "#fff7dd", src imageUrl] []
        ]
      , div [class "col-xs-12 col-md-6", css [textAlign left]] 
        [
          h3 [] [a [href link, css [textDecoration underline, textAlign left]] [text title]]
          , p [] [text content]
        ]
    ]



nixopsProject : Html Msg 
nixopsProject = div [] 
    [
        img [style "width" "100%", src "phoenix.png"][]
        ,styledP """Phoenix liveview enables server-side-rendered ''single-page'' web pages. It does this by the power of a brand-new technology (not really) called websockets."""
        ,styledP """Liveview sets up a websocket connection between the client and the server.
                         Commands goes from the client, and an updated html is sent back.
                         Morphdom then seemlessly updates the view."""
            
        ,styledP """Why isn't this implemented in other backend languages you might wonder.
                    Well, mostly because websockets are statefull, and most languages are not
                    optimised for that."""

        ,styledP """In Elixir/Erlang state is trivial. This is because the latter is an (unintentional)
                    implementation of the actor model. Each websocket connection is an totally isolated
                    process (actor) that can only communicate with other actors through message passing.
                    I.e. Share by communicating, don't communicate by sharing (i'm looking at 
                    you  C++) """                
        ,styledP
                """
                NixOS is a declerative linux distribution built around the nix package manager.
                It enables, and encourages, users to declare their system in a version controlled
                config file. This makes rebuilding and duplication trivial. This is possible
                as nix packages are 100% declerative. Each package is built by pure functions,
                guaranteeing that it is identical each time it is built.
                """                
        ,styledP 
                """
                Nixops allows you to define a NixOS system, build it locally, and ship it
                to one or more remote NixOS machines.
                """
        ,subTitle "Whats wrong with Kubernetes?"
        , p [] [ text "Nothing! It seems that kubernetes is about to become the de-facto standard for hosting. At-least for companies that are worried about cloud-provider lock-in (which they should be). Also, with microk8s it's cheap to spin up a cluster for hobby projects as well. The reason I, currently, choose to use nixops for my projects are. "]
                ,ul [] [
                   li [] [text " No need for a container registry"]
                   ,li [] [text "Nix-shell on the default.nix makes a shell with all dependencies to build and run the project "]
                   ,li [] [text "I can run it on a VM without a load balancer (cheap)"]
                   ,li [] [text "I wanted to try something new"]

               ]
        , subTitle "Making the nix package derivation"
        , p [] 
            [ 
                text" Create the phoenix project with"
               ,Html.Styled.pre [] [code [] [text "mix phx.new --live --no-ecto"]]
               , text "Test it by running"
               ,Html.Styled.pre [] [code [] [text "mix phx.server and go to localhost:4000"]]
               , text "Try searching in the form and see server rendered dynamic content in action"
               , text """Now, we create a derivation for how nix should build this project.
                        Create a new file named default.nix
                        and fill in"""
               , styledC """ 
with import <nixpkgs> {}; let
stadler_no = {}:
 stdenv.mkDerivation rec {
  name = "stadler_no";
  src = ./.;
  buildInputs = [elixir git nodejs];

  #Certs are only needed if you pull repos directly from github

  buildPhase = ''
  mkdir -p $PWD/.hex
  export HOME=$PWD/.hex
  export GIT_SSL_CAINFO=/etc/ssl/certs/ca-certificates.crt
  export SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt
  mix local.rebar --force
  mix local.hex --force
  mix deps.get
  MIX_ENV=prod mix compile
  npm install --prefix ./assets
  npm run deploy --prefix ./assets
  mix phx.digest
  MIX_ENV=prod mix release

  '';
  installPhase = ''
    mkdir -p $out
    cp -rf _build/prod/rel/stadler_no/* $out/

  '';
};
in
stadler_no"""
            , styledP "There are multiple things going on here. For a more thorough explanation of nix packages I recommend this this blogpost by Christine Dodrill. What you need to pay attention to is "
            , ol [] [li [] [text "buildInputs -- (The requirements to build) "], li [] [text "buildPhase -- (A bash recipe for building phoenix releases) "], li [] [text "install phase -- (Copying the binary to an artifact directory) " ]]
            , p [] [text "You can test it by running "]
            , styledC " nix-build default.ex --option sandbox false"
            , styledP  "The latter is needed as we fetch deps from the internet. If successfull you should see a result folder in your directory. "
            , styledP "Note: the default.nix can also be used to set up a development environment. Simply run "
            , styledC " nix-shell defualt.nix"
            , styledP "and you will drop into a shell with all the needed dependencies installed. This is supernice when multiple devs are working on the same project "
            , subTitle "Making the service" 
            , styledP "nix-build only compile the code, is does not run it. Next step is to set up a service for that. A systemd service to be exact. Create the file service.nix and fill it with "
            , styledC """
{config, lib, pkgs, ...}:
    let

        #Build the derivation described in default.nix
    	stadler_no_build = pkgs.callPackage ./default.nix {};
    	cfg = config.services.stadler_no;

in {

    #Set configurable options
    options.services.stadler_no.enable = lib.mkEnableOption "stadler_no";
    options.services.stadler_no.port = lib.mkOption {
        type = lib.types.int;
        default = 4000;
    };

    config = lib.mkIf cfg.enable {

    networking.firewall.allowedTCPPorts = [ cfg.port ];


    #Define the service
    systemd.services.stadler_no = {
        description = "My home page stadler no";
        environment = {
            HOME="/root";
            PORT="${toString cfg.port}";
            RELEASE_TMP="/tmp";
            #Secrets will be injected at a later time
            ADMIN_PWD=''$(cat /run/keys/admin_pwd)'';
            Stadler_NO_SKB=''$(cat /run/keys/stadler_no_skb)'';
            Stadler_NO_LV_SS=''$(cat /run/keys/stadler_no_lv_ss)'';
            };

     after = [ "network-online.target" ];
     wantedBy = [ "network-online.target" ];


    serviceConfig = {
        DynamicUser = "true";
        PrivateDevices = "true";
        ProtectKernelTunables = "true";
        ProtectKernelModules = "true";
        ProtectControlGroups = "true";
        RestrictAddressFamilies = "AF_INET AF_INET6";
        LockPersonality = "true";
        RestrictRealtime = "true";
        SystemCallFilter = "@system-service @network-io @signal";
        SystemCallErrorNumber = "EPERM";

	#This command is run when the systemd service starts
        ExecStart = "${stadler_no_build}/bin/stadler_no start";
        Restart = "always";
        RestartSec = "5";
    };
    };
};
}"""
            , styledP "Commit the code and push it to Gitlab"
            , subTitle "Nixops"
            , styledP "It's time to set up the server. You can naturally use the same repo as before, but I like creating a new one as I have many projects in the same deployment. "
            , styledP "Create a new directory named nixops_do, and generate a ssh keypair here. Be sure to place the keys in the nixops_do, and not overwrite the one in ~/.ssh. Keep the passphrase empty "
            , p [] 
                [
                    text "After this, go to digitalocean.com, create an ubuntu droplet, and add the newly generated key under authorized hosts "
                    , a [href "https://www.digitalocean.com/docs/droplets/how-to/add-ssh-keys/to-existing-droplet/"] [text "Tutorial"]
                ]
            , styledP "Make three empty files, nixops_do.nix, nixops_do_hw.nix, and nixops_do_secrets.nix The first will describe what is running on the machine, the second describes the machine, and the latter injects secrets as env variables "
            , b [] [text "nixops_do.nix"]
            , styledC """
## Fetch service from gitlab
stadler_no = builtins.fetchGit {
      url = "https://gitlab.com/akselsk/stadler_no";
      ref = "master";
      rev = "82b0e190d10c70c1878d1d61e3b7e1618f14d822";
};

in
    let backend= { config, pkgs, ... }: {
  	environment.systemPackages = with pkgs; [cacert git vim ];

	# Add public key to enable updates
  	users.users.root = {
    	openssh.authorizedKeys.keys = ["ssh-ed25519 **pubkey** aksel@stadler.no"];
  	};

    services.openssh.enable = true;
  	#Import the service file made previously
  	imports = [
      	"${stadler_no.outPath}/service.nix"

  	];
        networking.firewall.allowedTCPPorts = [ 22 4343 80 443 ];
        # Enable the service
  	services.stadler_no.enable = true;
  	services.stadler_no.port= 4002;
  	services.openssh.permitRootLogin = "yes";

 	#Allow the erlang vm to write error logs to disk
	systemd.tmpfiles.rules = [
  	"d /root/tmp 0755 root root"
	];

	#Start nginx procy pass
        services.nginx = {
            enable = true;
            recommendedProxySettings = true;
            recommendedTlsSettings = true;

            virtualHosts."stadler.no" =  {
              enableACME = true;
              forceSSL = true;
              locations."/" = {
                proxyPass = "http://127.0.0.1:4002";
                proxyWebsockets = true; # needed if you need to use WebSocket
              };
            };

        };

        security.acme.acceptTerms = true;
	security.acme.certs = {
  	"stadler.no".email = "aksel@stadler.no";
	};

    	};


in

{

  network.description = "Test server";
  #The server name, must have a corresponding hardware name
  asknixops1= backend;

  #If desireably to deploy to multiple machines
  #asknixops2= backend
}"""
            , b [] [text "nixops_do_hw.nix"]
            , styledC """
  # Hardware config
 let machine = { config, pkgs, ... }: {
    deployment.targetEnv = "digitalOcean";
    deployment.digitalOcean.enableIpv6 = true;
    deployment.digitalOcean.region = "ams3";
    deployment.digitalOcean.size = "s-1vcpu-1gb";
    deployment.digitalOcean.vpc_uuid= "default-ams3";
   # deployment.digitalOcean.authToken = "Doesn't seem to work";
  };
in
{
  resources.sshKeyPairs.ssh-key = {
    publicKey = builtins.readFile ./tstKey.pub;
    privateKey = builtins.readFile ./tstKey;
  };

  asknixops1= machine; #Much mach a corresponding server

  #asknixops2 = machine;

}"""
            , b [] [text "nixops_secrets.nix"]
            , styledC """
  asknixops1=
    { config, pkgs, ... }:
    {
      deployment.keys.admin_pwd.text = "***";
      deployment.keys.stadler_no_skb = "**";
      deployment.keys.stadler_no_lv_ss = "**";
    };
}"""
            , styledP "Create deployment by running"
            , styledC "nixops create ./nixops_do.nix ./nixops_do_hw.nix ./nixops_do_secrets.nix -d nixops_do "
            , styledP "And ship it with"
            , styledC "nixops deploy -d nixops_do "
            , styledP "For debugging you can ssh into the machine with"
            , styledC "nixops ssh *deployname* *machine_name*  "
            , styledP "And print logs with "
            , styledC "systemctl status *service_name* or journalctl -u *service_name* "


            ]
        



    ]


ledProject : Html Msg
ledProject =  div [] 
    [
        img [style "width" "100%", src "saunandtermo.png"] []
        ,p [] [text  "For Africa Burn 2019 my camp (Vagabonds) decided to gift a sauna to the community. This is a quick and dirty walkthrough for making the LED thermometer as seen on the picture. The hotter it gets the more LEDs are lit, and redder they become. In the picture the temperature  is 60°C and the range is 0-80°C"]
        , subTitle "Equipment"
        , Html.Styled.table [style "width" "100%", css[borderCollapse collapse, color theme.primText]] 
            [
                tr [] 
                [
                    styledTh "What"
                    ,styledTh "Which"
                    ,styledTh "Link"
                ],
                tdTrio "Power Supply " "PHEVOS 5v 12A Dc Universal Switching Power Supply for Raspberry PI Models,CCTV "  "tbd"
                ,tdTrio "Micro Controller" "Raspberry pi 3" "tb"
                ,tdTrio "Thermometer" "Vktech 2M Waterproof Digital Temperature Temp Sensor Probe DS18b20" "tbd"
                ,tdTrio "Copper Wire" "UL1015 Commercial Copper Wire, Bright, Red, 22 AWG, 0.0253 Diameter, 100' (As the leds draws about 6AMP its nice/wise to have a bit more then the standard RPI jumpers)" "tbd"
                ,tdTrio "LEDS" "ALITOVE WS2812B Individually Addressable LED Strip Light 5050 RGB 16.4ft 300 LED Pixel Flexible Lamp Tube Waterproof IP67 Black PCB DC 5V" "tbd"



            ]
        , subTitle "Wiring"
        ,img [style "width" "100%", style "background-color" "white", src "wiring.png"] []
        ,subTitle "Code"
        ,p [] 
        [
            text "The code is rather strighforward as it builds upon" 
            ,a [href "https://learn.adafruit.com/neopixels-on-raspberry-pi/python-usage"] [ text "Neo Pixel"]
            ,text "and"
            ,a [href "https://github.com/timofurrer/w1thermsensor"] [ text "w1Thermsensor."]
            ,text "Only small modifications where needed from the NeoPixel examples. "
        ]

        ,Html.Styled.pre [] [
            code [] [
        text """#!/usr/bin/env python3

import time
from rpi_ws281x import *
import argparse
import math
from w1thermsensor import W1ThermSensor
sensor = W1ThermSensor()

MAX_TEMP = 100
MIN_TEMP = 0


# LED strip configuration:
LED_COUNT      = 300      # Number of LED pixels.
MAX_COLOR_LED = 200
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53

def getColor(lednr):
    red = math.floor(lednr/MAX_COLOR_LED * 255)
    print("red is",red)
    print("lednr is",lednr)
    if red > 255:
        red = 255

    blue = 255-red

    if blue > red:
        green = 255 - blue
    else:
        green = 255 -red
        red = 255
        blue = 0

    return Color(red,green,blue)

def SetColor(temp):
    nr_of_leds =math.floor(LED_COUNT*temp/MAX_TEMP)
    print("nr of leds is",nr_of_leds)
    for i in range(0,LED_COUNT):
        if i < nr_of_leds:
            color = getColor(i)
        else:
            color = Color(0,0,0)
        strip.setPixelColor(i,color)
        strip.show()


# Main program logic follows:
if __name__ == '__main__':
    # Process arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--clear', action='store_true', help='clear the display on exit')
    args = parser.parse_args()

    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
    # Intialize the library (must be called once before other functions).
    strip.begin()

    print ('Press Ctrl-C to quit.')
    if not args.clear:
        print('Use "-c" argument to clear LEDs on exit')

    try:
        while True:
            SetColor(math.floor(sensor.get_temperature()))
            time.sleep(15)


    except KeyboardInterrupt:
        if args.clear:
            colorWipe(strip, Color(0,0,0), 10)
            """

            ]

        ]
        
    ]


styledTh : String -> Html Msg
styledTh txt = th [css [textAlign left]] [text txt]

tdTrio : String -> String -> String -> Html Msg
tdTrio a b c = tr [] 
    [
        td [] [text a]
        ,td [] [text b]
        ,td [] [text c]

    ]
